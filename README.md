# PHP Challenge

[Desenvolvimento do desafio](https://raw.githubusercontent.com/jusbrasil/careers/master/php-fullstack/challenge.md)

## Rodar em uma linha

```
bash <(wget -qO- https://bitbucket.org/gmonnerat/jus/raw/master/run.sh)
```

## Como instalar a imagem Docker?
```
docker build -t jus .
```

## Como rodar os testes unit�rios?
```
docker run --rm -it jus /challenge/vendor/bin/phpunit
```

## Como rodar e acessar?
```
docker run -p8000:8000 --rm -it jus
```

ou

```
docker run --net=host --rm -it jus
```

- Estou utilizando --rm para remover a imagem e n�o criar lixo
- Estou utilizando --net=host para acessar via SSH Forwarding em um servidor
- Alguns pontos interessantes n�o foram considerados, por exemplo o tamanho da imagem docker. Outras distribui��es, como alpine, podem gerar imagens menores


| Method   | URI                | Name     | Action                                       | Middleware   |
|----------|--------------------|----------|----------------------------------------------|--------------|
| GET|HEAD | /                  |          | Closure                                      | web          |
| POST     | api/charge         |          | App\Http\Controllers\UserController@charge   | api          |
| GET|HEAD | api/users          |          | App\Http\Controllers\UserController@index    | api          |
| POST     | api/users          |          | App\Http\Controllers\UserController@register | api          |
| GET|HEAD | api/users/{id}     |          | App\Http\Controllers\UserController@show     | api          |
| GET|HEAD | register           | register | Closure                                      | web          |
| GET|HEAD | user/{id}/checkout | checkout | App\Http\Controllers\UserController@checkout | web          |
| GET|HEAD | user/{id}/status   | status   | App\Http\Controllers\UserController@status   | web          |


