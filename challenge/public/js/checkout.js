/*global document, axios, FormData, CPF, Handlebars*/
(function(document, axios, FormData, Handlebars) {
  "use strict";
  var template = Handlebars.compile(
    document.getElementById("message-template").innerText);

	function cleanUpElement(el) {
    var elParent = el.parentElement,
        childList = elParent.childNodes,
        i;
    elParent.className = "form-group";
    for (i in childList) {
      if (childList.hasOwnProperty(i) && childList[i].tagName == "SPAN") {
        elParent.removeChild(childList[i]);
        return cleanUpElement(el);
      }
    }
    return;
  }

  function addClassToElement(el, message, className) {
    var elParent = el.parentElement,
        span;

    cleanUpElement(el);
    elParent.className += " " + className;
    span = document.createElement("span");
    span.className = "help-block";
    span.innerHTML = message;
    elParent.appendChild(span);
    return;
  }

  function addErrorToElement(el, message) {
    return addClassToElement(el, message, "has-error");
  }


  function setTotalAmount(evt) {
    var el = evt.target,
      option = el.options[el.selectedIndex],
      totalAmountField = document.querySelector("input[name='total_amount']");
    // We should get this values from backend but for now, it's ok
    if (option.value === "basic") {
      totalAmountField.value = "10";
    } else if (option.value === "advanced") {
      totalAmountField.value = "20";
    } else {
      totalAmountField.value = "30";
    }
  }
	function handleForm(evt) {
    var formData;
		evt.preventDefault();
		document.querySelector("button[type='submit']").disabled = true;
	  formData = new FormData(evt.target);
    axios.post('/api/charge', formData)
    .then(function (response) {
      document.querySelector(".content").innerHTML = template({
        message: "Pagamento realizado com sucesso",
      }); 
    })
    .catch(function (error) {
      var errors = error.response.data.errors;
      if (errors) {
        Object.keys(errors).forEach(function(key){
          addErrorToElement(document.getElementById(key), errors[key][0]);
        });
      }
      if (error.response.data.payment_status == "failed"){
        document.querySelector(".content").innerHTML = template({
          message: "Pagamento falhou",
          link: {
            href:location.href,
            title: "Link para tentar novamente"}
        }); 
      }
    })
    .then(function () {
      document.querySelector("button[type='submit']").disabled = false;
    })

	}

  document.addEventListener("DOMContentLoaded", function(){
    document.getElementById("plan").addEventListener("change", setTotalAmount);
    document.querySelector("form").addEventListener("submit", handleForm);
  });
}(document, axios, FormData, Handlebars));
