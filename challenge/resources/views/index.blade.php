<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PHP Desafio</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="/css/all.css">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
              <a href="{{ route('register') }}">Cadastrar</a>
            </div>
            <div class="content">
                <div class="title m-b-md">
                    PHP Desafio
                </div>

                <div class="links">
                    <a href="https://github.com/jusbrasil/careers/blob/master/php-fullstack/challenge.md">Desafio</a>
                </div>
            </div>
        </div>
    </body>
</html>
