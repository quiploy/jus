<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Checkout - {{ $user->name }}</title>
        <script id="message-template" type="text/x-handlebars-template">
					<div class="title m-b-md">
					  @{{message}}
					</div>
					@{{#if link}}
					<div class="links">
							<a href="@{{link.href}}">@{{link.title}}</a>
					</div>
          @{{/if}}
        </script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/all.css">

				<!-- jQuery library -->
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

				<!-- Popper JS -->
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

				<!-- Latest compiled JavaScript -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.0/handlebars.min.js"></script>
        <script src="/js/checkout.js"></script>        

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
              <form method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{ $user->id }}">
								<div class="row">
									<div class="col-lg-6">
										<h3>Resumo do cadastro</h3>
										<div class="form-group">
                      Nome: {{ $user->name }}
										</div>
										<div class="form-group">
                      Email: {{ $user->email }}
                    </div>
										<div class="form-group">
                      CPF: {{ $user->cpf }}
                    </div>
										<div class="form-group">
											Endereço: {{ $address->print() }}
                    </div>
                    <div class="form-group">
                      @foreach ($phones as $phone)
                      <span>Telefone: {{ $phone->phone }}</span><br/>
                      @endforeach
                    </div>
									</div>
									<div class="col-lg-6">
										<h3>Checkout</h3>
                    <div class="form-group">
                      <label class="control-label" for="plan">Planos disponíveis</label>
                      <select name="plan" size="1" class="form-control" title="" id="plan" required>
                        <option value="" selected>-------</option>
                        <option value="basic">Básico - R$ 10,00</option>
                        <option value="advanced">Avançado - R$ 20,00</option>
                        <option value="custom">Sob demanda - R$ 30,00</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="total_amount">Valor a pagar</label>
                      <input class="form-control" id="total_amount" name="total_amount" readonly>
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="credit_card">Número do cartão</label>
                      <input class="form-control" id="credit_card" name="credit_card">
                    </div> 
									</div>
								</div>
								<div class="form-group">
									<button style="cursor:pointer" type="submit" class="btn btn-primary">Cadastrar</button>
								</div>
              </form>
            </div>
        </div>
    </body>
</html>
